/**
 * Created by mivanova on 7.10.2015 �..
 */
angular.module('myApp.offers', ['ngRoute'])

    .config(['$routeProvider', function($routeProvider) {
        $routeProvider.when('/offers', {
            templateUrl: 'offers/offers.html',
            controller: 'OffersCtrl'
        });
    }])

    .controller('OffersCtrl', ['$scope', 'CarsTemp', 'UserService', 'CarService', OffersCtrl]);

function OffersCtrl($scope, CarsTemp, UserService, CarService) {
    $scope.carsTemp = CarsTemp;
    UserService.getSearch().then(function success(response){
        CarService.getBySearch(response.data).then(function success(response){
            $scope.carsData = response.data;
            console.log( $scope.carsData);
        });
    });


}