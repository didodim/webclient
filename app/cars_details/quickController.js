'use strict';

angular.module('myApp.quick', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/quick_search', {
    templateUrl: 'cars_details/quick_search.html',
    controller: 'QuickCtrl'
  });
}])

.controller('QuickCtrl', ['$scope',  '$location', 'SearchConfig','UserService', QuickCtrl]);

function QuickCtrl($scope, $location, SearchConfig, UserService) {
  $scope.searchCar = SearchConfig;
  UserService.getSearch().then(function success(response){
    $scope.searchData = response.data;
  });


  $scope.year = (new Date().getFullYear()) - 20;
  var range = [];
  range.push($scope.year);
  for(var i=1;i<=20;i++) {
    range.push($scope.year + i);
  }
  $scope.years = range;
  $scope.curYear = $scope.years[$scope.years.length-1];

  $scope.price = 1000;
  var allPrice = [];
  allPrice.push($scope.price);
  for(var i=1000;i<=100000;i=i+1000) {
    allPrice.push($scope.price + i);
  }
  $scope.prices = allPrice;
  $scope.curPrice = $scope.prices[0];
  $scope.search = {type:"ebane"};

  $scope.searchSubmit = function (){
    console.log($scope.searchData);
    UserService.updateSearch($scope.searchData);
    $location.path('/offers');
  }

}
