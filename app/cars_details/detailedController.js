/**
 * Created by mivanova on 7.10.2015 �..
 */
angular.module('myApp.detailed', ['ngRoute'])

    .config(['$routeProvider', function($routeProvider) {
        $routeProvider.when('/detailed_search', {
            templateUrl: 'cars_details/detailed_search.html',
            controller: 'DetailedCtrl'
        });
    }])

    .controller('DetailedCtrl', ['$scope', '$location', 'SearchConfig','UserService', DetailedCtrl]);

function DetailedCtrl($scope, $location, SearchConfig, UserService) {
    $scope.searchCar = SearchConfig;
    UserService.getSearch().then(function success(response){
        $scope.searchData = response.data;
    });


    $scope.year = (new Date().getFullYear()) - 20;
    var range = [];
    range.push($scope.year);
    for(var i=1;i<=20;i++) {
        range.push($scope.year + i);
    }
    $scope.years = range;
    $scope.curYear = $scope.years[$scope.years.length-1];

    $scope.price = 1000;
    var allPrice = [];
    allPrice.push($scope.price);
    for(var i=1000;i<=100000;i=i+1000) {
        allPrice.push($scope.price + i);
    }
    $scope.prices = allPrice;
    $scope.curPrice = $scope.prices[0];

    $scope.searchSubmit = function (){
        console.log($scope.searchData);
        UserService.updateSearch($scope.searchData);
        $location.path('/offers');
    }
    
}