/**
 * Created by mivanova on 7.10.2015 �.. s
 */
'use strict';

angular.module('myApp.statistics', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/statistics', {
            templateUrl: 'statistics/statistics.html',
            controller: 'StatisticsCtrl'
        });
    }])

    .controller('StatisticsCtrl', ['$scope', 'PropertyService', 'StatisticService', StatisticsCtrl]);

function StatisticsCtrl($scope, PropertyService, StatisticService) {

    $scope.kilometer = 10000;
    var range = [];
    range.push($scope.kilometer);
    for(var i=10000;i<=200000;i=i+1000) {
        range.push($scope.kilometer + i);
    }
    $scope.kilometers = range;
    $scope.curKilometer = $scope.kilometers[0];

    StatisticService.getHistogram('kilometers', $scope.curKilometer).then(function success(response) {
        $scope.statKm = response.data;

        $scope.statKeys = [];
        $scope.statValues = [];
        $scope.alll = [];
        angular.forEach($scope.statKm, function (value, key) {
            angular.forEach(value, function (value, key) {
                $scope.statKeys.push(key);
                $scope.statValues.push(value);
            });
        });

        $scope.labels = $scope.statKeys;
        $scope.data = $scope.statValues;
        console.log($scope.data);

        $scope.type = 'PolarArea';

        $scope.toggle = function () {
            $scope.type = $scope.type === 'PolarArea' ?
                'Pie' : 'PolarArea';
        };

        $scope.onClick = function (points, evt) {
            console.log(points, evt);
        };

    });

    $scope.getChangeData = function (curKilometer) {
        $scope.curKilometer = curKilometer;
        StatisticService.getHistogram('kilometers', $scope.curKilometer).then(function success(response) {
            $scope.statKm = response.data;

            $scope.statKeys = [];
            $scope.statValues = [];
            $scope.alll = [];
            angular.forEach($scope.statKm, function (value, key) {
                angular.forEach(value, function (value, key) {
                    $scope.statKeys.push(key);
                    $scope.statValues.push(value);
                });
            });

            $scope.labels = $scope.statKeys;
            $scope.data = $scope.statValues;
            console.log($scope.data);

            $scope.type = 'PolarArea';

            $scope.toggle = function () {
                $scope.type = $scope.type === 'PolarArea' ?
                    'Pie' : 'PolarArea';
            };

            $scope.onClick = function (points, evt) {
                console.log(points, evt);
            };

        });
    };

    $scope.price = 1000;
    var allPrice = [];
    allPrice.push($scope.price);
    for(var i=1000;i<=50000;i=i+1000) {
        allPrice.push($scope.price + i);
    }
    $scope.prices = allPrice;
    $scope.curPrice = $scope.prices[0];

    StatisticService.getHistogram('price', $scope.curPrice).then(function success(response) {
        $scope.statPrice = response.data;

        $scope.statKeysPrice = [];
        $scope.statValuesPrice = [];
        angular.forEach($scope.statPrice, function (value, key) {
            angular.forEach(value, function (value, key) {
                $scope.statKeysPrice.push(key);
                $scope.statValuesPrice.push(value);
            });
        });

        $scope.labelsPrice = $scope.statKeysPrice;
        $scope.dataPrice = $scope.statValuesPrice;

        $scope.type = 'PolarArea';

        $scope.toggle = function () {
            $scope.type = $scope.type === 'PolarArea' ?
                'Pie' : 'PolarArea';
        };

        $scope.onClick = function (points, evt) {
            console.log(points, evt);
        };

    });

    $scope.getChangePrice = function (curPrice) {
        $scope.curPrice = curPrice;
        StatisticService.getHistogram('price', $scope.curPrice).then(function success(response) {
            $scope.statPrice = response.data;

            $scope.statKeysPrice = [];
            $scope.statValuesPrice = [];
            angular.forEach($scope.statPrice, function (value, key) {
                angular.forEach(value, function (value, key) {
                    $scope.statKeysPrice.push(key);
                    $scope.statValuesPrice.push(value);
                });
            });

            $scope.labelsPrice = $scope.statKeysPrice;
            $scope.dataPrice = $scope.statValuesPrice;

            $scope.type = 'PolarArea';

            $scope.toggle = function () {
                $scope.type = $scope.type === 'PolarArea' ?
                    'Pie' : 'PolarArea';
            };

            $scope.onClick = function (points, evt) {
                console.log(points, evt);
            };

        });
    };


    $scope.year = 1;
    var allYear = [];
    allYear.push($scope.year);
    for(var i=1;i<=10;i=i+1) {
        allYear.push($scope.year + i);
    }
    $scope.years = allYear;
    $scope.curYear = $scope.years[0];
    StatisticService.getHistogram('year',  $scope.curYear).then(function success(response) {
        $scope.statYear = response.data;

        $scope.statKeysYear = [];
        $scope.statValuesYear = [];
        angular.forEach($scope.statYear, function (value, key) {
            angular.forEach(value, function (value, key) {
                $scope.statKeysYear.push(key);
                $scope.statValuesYear.push(value);
            });
        });

        $scope.labelsYear = $scope.statKeysYear;
        $scope.dataYear = $scope.statValuesYear;

        $scope.type = 'PolarArea';

        $scope.toggle = function () {
            $scope.type = $scope.type === 'PolarArea' ?
                'Pie' : 'PolarArea';
        };

        $scope.onClick = function (points, evt) {
            console.log(points, evt);
        };

    });

    $scope.getChangeYear = function (curYear) {
        $scope.curYear = curYear;
        StatisticService.getHistogram('year',  $scope.curYear).then(function success(response) {
            $scope.statYear = response.data;

            $scope.statKeysYear = [];
            $scope.statValuesYear = [];
            angular.forEach($scope.statYear, function (value, key) {
                angular.forEach(value, function (value, key) {
                    $scope.statKeysYear.push(key);
                    $scope.statValuesYear.push(value);
                });
            });

            $scope.labelsYear = $scope.statKeysYear;
            $scope.dataYear = $scope.statValuesYear;

            $scope.type = 'PolarArea';

            $scope.toggle = function () {
                $scope.type = $scope.type === 'PolarArea' ?
                    'Pie' : 'PolarArea';
            };

            $scope.onClick = function (points, evt) {
                console.log(points, evt);
            };

        });

    }

}