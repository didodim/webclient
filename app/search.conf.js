'use strict';
angular.module('myApp').constant('SearchConfig', {
    "cars": {
        "Vw": ["Bora", "Caddy", "Golf", "Jetta", "Passat", "Polo", "Vento", "Touran", "Phaeton", "Up", "Golf Plus", "Passat CC", "S8", "Tt", "Golf Variant", "CC"],
        "Audi": ["A4", "A3", "A5", "A6", "A7", "A8", "S2", "S3", "S4", "S5", "S6", "S7", "S8", "Tt", "Allroad", "R8"],
        "Mercedes": ["A180", "A140", "B180", "B200", "C200", "C220", "C270", "E200", "E220", "E270", "E320", "E350", "S63 AMG", "S320", "S350", "S500"],
        "Bmw": ["318", "320", "330", "520", "525", "530", "730", "740", "750"],
        "Opel": ["Agila", "Ampera", "Astra", "Calibra", "Combo", "Corsa", "Vento", "Diplomat", "Gt", "Insignia", "Manta", "Meriva", "Tigra", "Vectra", "Zafira", "Signum"]
    },
    "types": ["Van", "Cabriolet", "Emergency bus", "Coupe", "Hearse", "Stretch Limousine", "Estate Car", "Saloon", "Minivan", "Truck", "Hatchback"],
    "suv": {
        "Vw": ["Amarok", "Taro", "Tiguan", "Touareg"],
        "Audi": ["Q3", "Q5", "Q7"],
        "Mercedes": ["ML270", "ML320", "G500"],
        "Bmw": ["X5", "X6"],
        "Opel": ["Campo", "Frontera", "Mokka", "Antara"]
    },
    "engines": ["Diesel", "Petrol", "Hybrid", "Electric"],
    "colors": ["Graphite Gray", "Light Blue", "Pasty", "Orange", "Tobacco", "Black", "Grey", "Dark blue", "Beige", "White", "Pearl", "Pink", "Dark Red", "Dark Blue", "Dark Red", "Chameleon", "Baeta", "Violet", "Sandy", "Beige", "Sandy", "Silver", "Bronze", "Red", "Dark Green", "Red Wine", "Reseda", "Golden", "Banana", "Metallic", "Dark Grey", "Brown", "Purple", "Blue", "Red Wine", "Yellow", "Ocher", "Green", "Light Grey", "Ivory", "Red"],
    "transmissions": ["Manual", "Automatic"],
    "sources": ["cars.bg", "mobile.bg"]
});