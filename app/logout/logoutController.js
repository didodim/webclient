'use strict';

angular.module('myApp.logout', ['ngRoute'])
.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/logout', {
    templateUrl: 'logout/logout.html',
    controller: 'logoutCtrl',isAnonymous:true
  });

}])
.controller('logoutCtrl', ['$scope','AuthService','$location',function($scope,AuthService,$location) {

    var response = AuthService.logout();
    response.then(function successResponse(response) {
        $location.path('/home');
    },function errorResponse(response) {
        alert(response.data.message);
    });

}]);