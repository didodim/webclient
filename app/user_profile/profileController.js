/**
 * Created by mivanova on 7.10.2015 �..
 */
'use strict';

angular.module('myApp.profile', ['ngRoute'])

    .config(['$routeProvider', function($routeProvider) {
        $routeProvider.when('/profile', {
            templateUrl: 'user_profile/profile.html',
            controller: 'ProfileCtrl'
        });
    }])

    .controller('ProfileCtrl', ['$scope','UserService','$location',ProfileCtrl]);

function ProfileCtrl($scope,UserService,$location) {
    UserService.get().then(function success(response){
        $scope.user = response.data;
    });

    $scope.update= function(prop, val){
        var usr = {};
        usr[prop] = val;
        UserService.update(usr).then(function success(response){
            $scope.user = response.data;
        },function error(response){
          alert(response);
        });

    }

}