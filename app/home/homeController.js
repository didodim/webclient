'use strict';

angular.module('myApp.home', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/home', {
    templateUrl: 'home/home.html',
    controller: 'HomeCtrl',isAnonymous:true,
    menuHide:true
  });
}])

.controller('HomeCtrl', [function() {
}]);