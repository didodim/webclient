'use strict';

angular.module('myApp.login', ['ngRoute'])
.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/login', {
    templateUrl: 'login/login.html',
    controller: 'LoginCtrl',isAnonymous:true
  });

}])
.controller('LoginCtrl', ['$scope','AuthService','$location',function($scope,AuthService,$location) {

      $scope.update = function(user) {
          var def = AuthService.login(user.username,user.password);
          def.then(function(r){
              $location.path('/profile');
          }).catch(function(reason){
            alert('Wrong username or password');
          });
      };
}]);