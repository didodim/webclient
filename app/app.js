'use strict';

// Declare app level module which depends on views, and components
angular.module('myApp', [
    'angular-md5',
    'ngRoute',
    'xeditable',
    'chart.js',
    'myApp.home',
    'myApp.login',
    'myApp.profile',
    'myApp.detailed',
    'myApp.quick',
    'myApp.offers',
    'myApp.statistics',
    'myApp.register',
    'myApp.logout'
]).config(['$httpProvider', function ($httpProvider) {
    $httpProvider.defaults.withCredentials = true;
}]).config(['$routeProvider', function ($routeProvider) {
    $routeProvider.otherwise({redirectTo: 'home'});
}]).controller('HeaderController', ['$rootScope', '$scope', 'AuthService', '$location', HeaderController]);

function HeaderController($rootScope, $scope, AuthService, $location) {
    $rootScope.$on('$routeChangeStart', function (event, next) {
        $scope.menuHide = next.menuHide;
        $scope.isAnonymous = next.isAnonymous;
        AuthService.isLogin().then(function successResponse(response) {
            $rootScope.user=response.data;
            if(next.isAnonymous){
                $location.path('/profile');
            }
        },function errorResponse(response) {
            $scope.showLogin=true;
            if(!next.isAnonymous){
                $location.path('/home');
            }
        });
    });

    $scope.isActive = function (viewLocation) {
        return viewLocation === $location.path();
    };
}

