/**
 * Created by mariy on 10/9/2015.
 */
'use strict';

angular.module('myApp.register', ['ngRoute'])
    .config(['$routeProvider', function($routeProvider) {
        $routeProvider.when('/register', {
            templateUrl: 'register/register.html',
            controller: 'RegisterController',
            isAnonymous:true
        });

    }])
    .controller('RegisterController', ['$scope','AuthService','$location',RegisterController]);

function RegisterController($scope,AuthService,$location){
    $scope.register = function(user) {
        var response = AuthService.register(user);
        response.then(function successResponse(response) {
            $location.path('/profile');
        },function errorResponse(response) {
            alert('Not correct email!');
        });
    };
}