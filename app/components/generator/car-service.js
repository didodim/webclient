/**
 * Created by mariy on 14-Feb-16.
 */
'use strict';
var myApp = angular.module('myApp');
myApp.factory('CarService', ['$http','PropertyService', 'UserService', CarService] );

function CarService($http, PropertyService, UserService) {
    var carService = {};
    carService.getBySearch = function (searchData) {
        return $http.post(PropertyService.generator.url + "cars/search",searchData).
            success(function (data, status) {
                return {data: data, status: status, success: true};
            }).error(function (data, status) {
                return {data: data, status: status, success: false};
            });
    }
    return carService;
}