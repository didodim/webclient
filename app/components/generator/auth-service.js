/**
 * Created by dido on 10/7/15.
 */
'use strict';
var myApp = angular.module('myApp');
myApp.factory('AuthService',['$http','$q', 'PropertyService','UserService',AuthService] )

function AuthService($http, $q, PropertyService,UserService) {
    var authService = {};
    authService.login = function (username, password) {
        var def = $q.defer();
        $http.post(PropertyService.generator.url + "auth/login",{email:username,password:password})
            .then(function successCallback(response) {
                def.resolve(response.data);
            }, function errorCallback(response) {
                def.reject('Failed to login');
            });
        return def.promise;
    }

    authService.register = function (user) {
        return $http.post(PropertyService.generator.url + "auth/register",user).
        success(function (data, status) {
            return {data: data, status: status, success: true};
        }).error(function (data, status) {
            return {data: data, status: status, success: false};
        });
    };

    authService.logout = function () {
        return $http.delete(PropertyService.generator.url + "auth/logout").
        success(function (data, status) {
            return {data: data, status: status, success: true};
        }).error(function (data, status) {
            return {data: data, status: status, success: false};
        });
    };

    authService.isLogin = UserService.get;

    return authService;
}