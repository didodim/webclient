/**
 * Created by mivanova on 15.02.2016.
 */
'use strict';
var myApp = angular.module('myApp');
myApp.factory('StatisticService', ['$http', 'PropertyService', StatisticService] );

function StatisticService($http, PropertyService) {
    var statisticService = {};
    statisticService.getHistogram = function (type,interval) {
        return $http.get(PropertyService.generator.url + "/stats/histogram?type="+type+"&interval="+interval).
        success(function (data, status) {
            return {data: data, status: status, success: true};
        }).error(function (data, status) {
            return {data: data, status: status, success: false};
        });
    }
    return statisticService;
}

