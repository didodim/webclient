'use strict';
var myApp = angular.module('myApp');
myApp.factory('UserService', ['$http','PropertyService',UserService] );

function UserService($http, PropertyService) {
    var userService = {};
    userService.get = function () {
        return $http.get(PropertyService.generator.url + "user").
        success(function (data, status) {
            return {data: data, status: status, success: true};
        }).error(function (data, status) {
            return {data: data, status: status, success: false};
        });
    }

    userService.update = function (user) {
        return $http.post(PropertyService.generator.url + "user",user).
        success(function (data, status) {
            return {data: data, status: status, success: true};
        }).error(function (data, status) {
            return {data: data, status: status, success: false};
        });
    }

    userService.getSearch = function () {
        return $http.get(PropertyService.generator.url + "search").
        success(function (data, status) {
            return {data: data, status: status, success: true};
        }).error(function (data, status) {
            return {data: data, status: status, success: false};
        });
    }

    userService.updateSearch = function (quickSearch) {
        return $http.post(PropertyService.generator.url + "search",quickSearch).
        success(function (data, status) {
            return {data: data, status: status, success: true};
        }).error(function (data, status) {
            return {data: data, status: status, success: false};
        });
    }

    return userService;

}